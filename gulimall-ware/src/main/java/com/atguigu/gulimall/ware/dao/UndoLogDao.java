package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author gjt
 * @email juntao132@163.com
 * @date 2021-08-30 23:51:04
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
