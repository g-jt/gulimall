package com.atguigu.gulimall.member.dao;

import com.atguigu.gulimall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author gjt
 * @email juntao132@163.com
 * @date 2021-08-30 23:19:57
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
